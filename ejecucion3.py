import pygame
from raycaster import Raycaster
from math import cos, sin, pi

class Ejecutable3(object):

    def __init__(self,width, height):

        super().__init__()

        self.width = width
        self.height = height
        self.font = pygame.font.SysFont("Arial", 20)
        self.screen = pygame.display.set_mode((width,height), pygame.DOUBLEBUF | pygame.HWACCEL )
        self.clock = pygame.time.Clock()
        self.rayCaster = Raycaster(self.screen)
        self.rayCaster.load_map("map.txt")
        self.start()


    def updateFPS(self):
        fps = str(int(self.clock.get_fps()))
        fps = self.font.render(fps, 1, pygame.Color("white"))
        return fps

    def start(self):

        isRunning = True
        while isRunning:


            for ev in pygame.event.get():
                if ev.type == pygame.QUIT:
                    isRunning = False

                elif ev.type == pygame.KEYDOWN:
                    newX = self.rayCaster.player['x']
                    newY = self.rayCaster.player['y']
                    forward = self.rayCaster.player['angle'] * pi / 180
                    right = (self.rayCaster.player['angle'] + 90) * pi / 180

                    if ev.key == pygame.K_ESCAPE:
                        isRunning = False
                    elif ev.key == pygame.K_w:
                        newX += cos(forward) * self.rayCaster.stepSize
                        newY += sin(forward) * self.rayCaster.stepSize
                    elif ev.key == pygame.K_s:
                        newX -= cos(forward) * self.rayCaster.stepSize
                        newY -= sin(forward) * self.rayCaster.stepSize
                    elif ev.key == pygame.K_a:
                        newX -= cos(right) * self.rayCaster.stepSize
                        newY -= sin(right) * self.rayCaster.stepSize
                    elif ev.key == pygame.K_d:
                        newX += cos(right) * self.rayCaster.stepSize
                        newY += sin(right) * self.rayCaster.stepSize
                    elif ev.key == pygame.K_q:
                        self.rayCaster.player['angle'] -= self.rayCaster.turnSize
                    elif ev.key == pygame.K_e:
                        self.rayCaster.player['angle'] += self.rayCaster.turnSize

                    i = int(newX/self.rayCaster.blocksize)
                    j = int(newY/self.rayCaster.blocksize)

                    if self.rayCaster.map[j][i] == ' ':
                        self.rayCaster.player['x'] = newX
                        self.rayCaster.player['y'] = newY




            self.screen.fill(pygame.Color("saddlebrown"))
            # Techo
            self.screen.fill(pygame.Color("saddlebrown"),(int(self.width / 2), 0,  int(self.width / 2), int(self.height / 2)))

            # Piso
            self.screen.fill(pygame.Color("dimgray"), (int(self.width / 2), int(self.height / 2),  int(self.width / 2), int(self.height / 2)))

            self.rayCaster.render()

            # FPS
            self.screen.fill(pygame.Color("black"), (0, 0, 40, 30))
            self.screen.blit(self.updateFPS(), (0, 0))

            pygame.display.update()
            self.clock.tick(60)