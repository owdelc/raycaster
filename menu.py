
import sys
import pygame
from ejecucion import Ejecutable
from ejecucion2 import Ejecutable2
from ejecucion3 import Ejecutable3
from pygame.locals import *
from pygame import mixer


background_image = pygame.image.load('fondo.jpg')

width = 1000
height = 500

class Menu_principal(object):
    def __init__(self):

        pygame.init()
        self.screen = pygame.display.set_mode((width, height), pygame.DOUBLEBUF | pygame.HWACCEL)
        self.Fuente_titulo = pygame.font.SysFont("Arial", 45)
        self.fuente_boton = pygame.font.SysFont("Arial", 35)
        self.estado = 0
        self.ejecutar()


    def mostrar_texto(self, texto, fuente, color, surf, x, y):
        textobj = fuente.render(texto, 1, color)
        textrect = textobj.get_rect()
        textrect.topleft = (x, y)
        surf.blit(textobj, textrect)

    def mostrar_fondo(self):
        textura = pygame.transform.scale(background_image, (width, height))
        rect = textura.get_rect()
        self.screen.blit(textura, rect)

    def crear_rect(self, width, height, border, color, border_color):
        surface = pygame.Surface((width+border*2, height+border*2), pygame.SRCALPHA)
        pygame.draw.rect(surface, color, (border, border, width, height), 0)
        for i in range(1, border):
            pygame.draw.rect(surface, border_color, (border-i,border-i, width+5, height+5), 1)
        return surface

    def ejecutar(self):
        mixer.music.load('anthem.mp3')
        mixer.music.play(-1)


        while 1:

            self.screen.fill((0, 0, 0))
            self.mostrar_fondo()
            pygame.draw.rect(self.screen,(0, 0, 0),pygame.Rect(345, 40, 350, 75),  border_radius=0)
            self.mostrar_texto('Finding Fuhrer', self.Fuente_titulo,(255, 162, 0), self.screen, 375, 50)

            mx, my = pygame.mouse.get_pos()

            b1 = pygame.Rect(400, 200, 240, 75)
            b2 = pygame.Rect(400, 300, 240, 75)
            b3 = pygame.Rect(400, 400, 240, 75)
            b4 = pygame.Rect(750, 400, 150, 75)

            if b1.collidepoint((mx, my)):
                if self.click:
                    Ejecutable(width,height)

            if b2.collidepoint((mx, my)):
                if self.click:
                    Ejecutable2(width, height)

            if b3.collidepoint((mx, my)):
                if self.click:
                    Ejecutable3(width,height)

            if b4.collidepoint((mx, my)):
                if self.click:
                    pygame.quit()
                    sys.exit()

            self.click = False


            pygame.draw.rect(self.screen,(0, 0, 0),b1,  border_radius=0)
            self.mostrar_texto('Go Berlin', self.fuente_boton,(255, 162, 0), self.screen, 450, 220)

            pygame.draw.rect(self.screen,(0, 0, 0),b2,  border_radius=0)
            self.mostrar_texto('Go Munich', self.fuente_boton,(255, 162, 0), self.screen, 440, 315)

            pygame.draw.rect(self.screen,(0, 0, 0),b3,  border_radius=0)
            self.mostrar_texto('Go Frankfurt', self.fuente_boton,(255, 162, 0), self.screen, 425, 420)

            pygame.draw.rect(self.screen,(0, 0, 0),b4,  border_radius=0)
            self.mostrar_texto('Quit', self.fuente_boton,(255, 162, 0), self.screen, 790, 420)


            for event in pygame.event.get():

                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()

                elif event.type == MOUSEBUTTONDOWN:
                    if event.button == 1:
                        self.click = True

                elif event.type == KEYUP:
                    if self.estado > 0:
                        self.estado -= 1
                    else:
                        self.estado = 0

                elif event.type == KEYDOWN:
                    if self.estado < 3:
                        self.estado += 1
                    else:
                        self.estado = 3

                elif event.type == K_KP_ENTER:
                    if self.estado == 0:
                        Ejecutable(width, height)

                    elif self.estado == 1:
                        pygame.quit()
                        sys.exit()

                elif event.type == K_SPACE:

                    if self.estado == 0:
                        Ejecutable(width, height)

                    elif self.estado == 1:
                        pygame.quit()
                        sys.exit()



            pygame.display.update()

